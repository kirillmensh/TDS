#include "../Character/StatusSystem.h"
#include "../TDS.h"

void UStatusSystem::EncreaseStamina(float StaminaStep)
{
	if (StaminaCurrent < StaminaMax) StaminaCurrent += StaminaStep;
}

void UStatusSystem::DecreaseStamina(float StaminaStep)
{
	if (StaminaCurrent >= 0) StaminaCurrent -= StaminaStep;
}


const float UStatusSystem::GetStamina()
{
	return StaminaCurrent;
}
