#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "StatusSystem.generated.h"

UCLASS()
class TDS_API UStatusSystem : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

private:

	float StaminaCurrent = 100.f;
	float StaminaMax = 100.f;

public:
	
	
	//UStatusSystem();

	UFUNCTION(BlueprintCallable)
	void EncreaseStamina(float StaminaStep);

	UFUNCTION(BlueprintCallable)
	void DecreaseStamina(float StaminaStep);

	UFUNCTION(BlueprintCallable)
	const float GetStamina();

};
